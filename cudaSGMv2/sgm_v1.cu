//#include "sgm.cuh"
//#include <thrust\version.h>
//
//using namespace cv;
//using namespace std;
//
///*********************************************************
//calculateDisparity() abstracts the internal working of SGM
//**********************************************************/
//void SGM::calculateDisparity()
//{
//	//clock_t begin = clock();
//	//clock_t end = clock();
//	//double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
//	//cout << "Time taken: " << elapsed_secs << endl;
//
//	for (paramsHost->imageSectionBeingProcessed = 0; paramsHost->imageSectionBeingProcessed < paramsHost->numberOfImageSections; ++paramsHost->imageSectionBeingProcessed)
//	{
//		SplitImagesIntoSections();
//		PreProcessAndUploadImagesToDevice();
//		PixelwiseCostCalculation();
//
//		AggregateCosts();
//
//		DisparityCalculation();
//		StitchImageSections();
//		ResetArrays();
//
//		cout << "Image Section processed: " << paramsHost->imageSectionBeingProcessed + 1 << " / " << paramsHost->numberOfImageSections << endl;
//	}
//	PostProcessAndGenerateMap();
//}
//
///*********************************************************
//Initialization of all the variables and arrays needed
//for the running of the program
//**********************************************************/
//SGM::SGM(std::string leftFilepath, std::string rightFilepath, std::string groundTruthFilePath, std::string maskFilePath, std::string subfolderFilepath, AlgorithmVariantToRun variant)
//{
//	SetGPU();
//
//	// Reading Images
//	leftHost = imread(leftFilepath, CV_8UC1);
//	rightHost = imread(rightFilepath, CV_8UC1);
//
//	if (!leftHost.data || !rightHost.data)
//	{
//		cout << "Images could not be read";
//		exit(EXIT_FAILURE);
//	}
//
//	if (groundTruthFilePath.length() > 0)
//		groundTruth = imread(groundTruthFilePath, CV_8UC1);
//
//	if (groundTruthFilePath.length() == 0 || !groundTruth.data)
//		cout << "Ground truth not provided. Average Error will not be calculated." << endl;
//
//	if (maskFilePath.length() > 0)
//		mask = imread(maskFilePath, CV_8UC1);
//
//	if (maskFilePath.length() == 0 || !mask.data)
//		cout << "Mask not provided" << endl;
//
//	// Resizing image
//	Size FourK(4096, 2160), FullHD(1920, 1080), HDReady(1280, 720), SD(640, 480);
//	//resize(leftHost, leftHost, FourK); resize(rightHost, rightHost, FourK); if(groundTruth.data) resize(groundTruth, groundTruth, FourK); if(mask.data)  resize(mask, mask, FourK);
//	//resize(leftHost, leftHost, FullHD); resize(rightHost, rightHost, FullHD); if(groundTruth.data) resize(groundTruth, groundTruth, FullHD); if(mask.data)  resize(mask, mask, FullHD);
//	//resize(leftHost, leftHost, HDReady); resize(rightHost, rightHost, HDReady); if(groundTruth.data) resize(groundTruth, groundTruth, HDReady); if(mask.data)  resize(mask, mask, HDReady);
//	resize(leftHost, leftHost, SD); resize(rightHost, rightHost, SD); if (groundTruth.data) resize(groundTruth, groundTruth, SD); if (mask.data)  resize(mask, mask, SD);
//
//	disparityHost = Mat(leftHost.rows, leftHost.cols, CV_16SC1);
//
//	currentVariant = variant;
//
//	disparityFilePath = subfolderFilepath + "disparity" + getVariantName() + ".png";
//
//	/// Algorithm Parameters
//	// Initializing global algorithm parameters class object
//	paramsHost = new algorithmParameters;
//	paramsHost->columnsDisparityRangeRatio = 4;
//
//	disparityFilePath = subfolderFilepath + "disparity" + getVariantName() + to_string(paramsHost->columnsDisparityRangeRatio) + ".png";
//
//	paramsHost->disparityRange = leftHost.cols / paramsHost->columnsDisparityRangeRatio;
//
//	paramsHost->imageSectionWidth = leftHost.cols;
//	paramsHost->nColsXDisparityRange = paramsHost->imageSectionWidth * paramsHost->disparityRange;	// Used in indexing the flattened 3D device arrays
//
//	paramsHost->p1 = 32 * matchingCostBlockSize * matchingCostBlockSize;
//	paramsHost->p2 = 64 * matchingCostBlockSize * matchingCostBlockSize;
//
//	size_t free, total;
//	checkCudaError(cudaMemGetInfo(&free, &total), "Getting device properties");
//
//	size_t deviceMemory = free - (350000000.0f);	// Total memory - 350 MB for disparityDevice and other variables
//	size_t memoryRequiredForSGM = leftHost.rows * leftHost.cols;
//	memoryRequiredForSGM *= paramsHost->disparityRange * 4;
//
//	switch (currentVariant)
//	{
//	case AllPaths:
//		deviceMemory = (size_t)floorl(deviceMemory / 2);
//		paramsHost->numberOfImageSections = floorl(memoryRequiredForSGM % deviceMemory) > 0 ? (int)floorl(memoryRequiredForSGM / deviceMemory) + 1 : (int)floorl(memoryRequiredForSGM / deviceMemory);
//		break;
//	case StreamedAllPaths:
//		deviceMemory = (size_t)floorl(deviceMemory / 3);
//		paramsHost->numberOfImageSections = floorl(memoryRequiredForSGM % deviceMemory) > 0 ? (int)floorl(memoryRequiredForSGM / deviceMemory) + 1 : (int)floorl(memoryRequiredForSGM / deviceMemory);
//		break;
//	case StreamedHalfPaths:
//		deviceMemory = (size_t)floorl(deviceMemory / 5);
//		paramsHost->numberOfImageSections = floorl(memoryRequiredForSGM % deviceMemory) > 0 ? (int)floorl(memoryRequiredForSGM / deviceMemory) + 1 : (int)floorl(memoryRequiredForSGM / deviceMemory);
//		break;
//	}
//	cout << "\nRunning variant: " << (currentVariant == AllPaths ? "All Paths" : (currentVariant == StreamedAllPaths ? "Streamed All Paths" : "Streamed Half Paths")) << endl;
//	cout << "Image will be split into " << paramsHost->numberOfImageSections << " parts since we require " << currentVariant << " arrays, each of size " << deviceMemory / 1000000 << "MBytes each" << endl << endl;
//
//	paramsHost->imageSectionHeight = leftHost.rows / paramsHost->numberOfImageSections;
//
//	// Copying the parameters to the device
//	checkCudaError(cudaMalloc(&paramsDevice, sizeof(algorithmParameters)), "Algorithm parameters initialization");
//
//	checkCudaError(cudaMemcpy(&paramsDevice->columnsDisparityRangeRatio, &paramsHost->columnsDisparityRangeRatio, sizeof(int), cudaMemcpyHostToDevice));
//	checkCudaError(cudaMemcpy(&paramsDevice->disparityRange, &paramsHost->disparityRange, sizeof(int), cudaMemcpyHostToDevice));
//	checkCudaError(cudaMemcpy(&paramsDevice->numberOfImageSections, &paramsHost->numberOfImageSections, sizeof(int), cudaMemcpyHostToDevice));
//	checkCudaError(cudaMemcpy(&paramsDevice->imageSectionBeingProcessed, &paramsHost->imageSectionBeingProcessed, sizeof(int), cudaMemcpyHostToDevice));
//	checkCudaError(cudaMemcpy(&paramsDevice->imageSectionHeight, &paramsHost->imageSectionHeight, sizeof(int), cudaMemcpyHostToDevice));
//	checkCudaError(cudaMemcpy(&paramsDevice->imageSectionWidth, &paramsHost->imageSectionWidth, sizeof(int), cudaMemcpyHostToDevice));
//	checkCudaError(cudaMemcpy(&paramsDevice->imageSectionStartX, &paramsHost->imageSectionStartX, sizeof(int), cudaMemcpyHostToDevice));
//	checkCudaError(cudaMemcpy(&paramsDevice->imageSectionStartY, &paramsHost->imageSectionStartY, sizeof(int), cudaMemcpyHostToDevice));
//	checkCudaError(cudaMemcpy(&paramsDevice->nColsXDisparityRange, &paramsHost->nColsXDisparityRange, sizeof(int), cudaMemcpyHostToDevice));
//	checkCudaError(cudaMemcpy(&paramsDevice->p1, &paramsHost->p1, sizeof(int), cudaMemcpyHostToDevice));
//	checkCudaError(cudaMemcpy(&paramsDevice->p2, &paramsHost->p2, sizeof(int), cudaMemcpyHostToDevice));
//
//	// Disparity Mat on the Device
//	disparityDevice = cuda::GpuMat(paramsHost->imageSectionHeight, paramsHost->imageSectionWidth, CV_16SC1, Scalar(0));
//
//	// Initializing Streams
//	checkCudaError(cudaStreamCreate(&Stream1), "Creating Stream 1");
//	checkCudaError(cudaStreamCreate(&Stream2), "Creating Stream 2");
//	checkCudaError(cudaStreamCreate(&Stream3), "Creating Stream 3");
//	checkCudaError(cudaStreamCreate(&Stream4), "Creating Stream 4");
//
//	// Cost Array Sizes
//	costArraySize = paramsHost->imageSectionHeight * paramsHost->imageSectionWidth * paramsHost->disparityRange;
//	aggregatedCostArraySize = paramsHost->imageSectionHeight * paramsHost->imageSectionWidth * paramsHost->disparityRange;
//
//	// Cost Arrays
//	cost.resize(costArraySize, 0);
//	switch (currentVariant)
//	{
//	case AllPaths:	// Only one Aggregated Cost Array required
//		aggregatedCostsPath1.resize(aggregatedCostArraySize, 0);
//		break;
//	case StreamedAllPaths:	// Two Aggregated Cost Arrays required
//		aggregatedCostsPath1.resize(aggregatedCostArraySize, 0);
//		aggregatedCostsPath2.resize(aggregatedCostArraySize, 0);
//		break;
//	case StreamedHalfPaths:	// Four Aggregated Cost Arrays required
//		aggregatedCostsPath1.resize(aggregatedCostArraySize, 0);
//		aggregatedCostsPath2.resize(aggregatedCostArraySize, 0);
//		aggregatedCostsPath3.resize(aggregatedCostArraySize, 0);
//		aggregatedCostsPath4.resize(aggregatedCostArraySize, 0);
//		break;
//	}
//	checkCudaError(cudaMemGetInfo(&free, &total), "Getting device properties again");
//}
//
///*********************************************************
//Check for:
//- Minimum CUDA Version of 6.0 (Unified Memory) TODO
//- In case of multiple GPUs, set it to the preferred
//choice from algorithmParameters
//- Makes sure the GPU has enough memory to run! TODO
//**********************************************************/
//void SGM::SetGPU()
//{
//	/* Adapted from NVIDIA Sample Code 'deviceQuery' */
//
//	int deviceCount = 0;
//	checkCudaError(cudaGetDeviceCount(&deviceCount), "Querying number of CUDA devices");
//
//	if (deviceCount == 0) {
//		cout << "There are no available device(s) that support CUDA" << endl;
//		cout << "Exiting Now" << endl;
//		exit(EXIT_FAILURE);
//	}
//
//	cout << endl << deviceCount << " device(s) available" << endl;
//	checkCudaError(cudaSetDevice(cudaDeviceToSelect), "CUDA device selection");
//
//	cudaDeviceProp deviceProp;
//	checkCudaError(cudaGetDeviceProperties(&deviceProp, cudaDeviceToSelect), "Querying selected CUDA device properties");
//	cout << "Device " << deviceProp.name << " selected" << endl;
//	size_t free, total;
//	checkCudaError(cudaMemGetInfo(&free, &total), "Getting device properties");
//	cout << "The GPU has " << free / 1000000 << "MBytes of free memory" << endl << endl;
//}
//
///*********************************************************
//The images are processed in multiple passes, each pass
//processing a section of the image
//**********************************************************/
//void SGM::SplitImagesIntoSections()
//{
//	paramsHost->imageSectionStartX = 0;
//	paramsHost->imageSectionStartY = paramsHost->imageSectionBeingProcessed * paramsHost->imageSectionHeight;
//
//	Rect roi = Rect(paramsHost->imageSectionStartX, paramsHost->imageSectionStartY, paramsHost->imageSectionWidth, paramsHost->imageSectionHeight);
//	leftSectionHost = leftHost(roi);
//	rightSectionHost = rightHost(roi);
//}
//
///*********************************************************
//Filters applied:
//- (Uncomment to apply) Gaussian filter
//- Sobel Filter
//**********************************************************/
//void SGM::PreProcessAndUploadImagesToDevice()
//{
//	// Applying the Gaussian Blur
//	//GaussianBlur(leftBlockHost, leftBlockHost, Size(3, 3), 0, 0);
//	//GaussianBlur(rightBlockHost, leftBlockHost, Size(3, 3), 0, 0);
//
//	//Sobel Operator
//	/// Generate grad_x and grad_y
//	int ddepth = CV_16SC1;
//	int scale = 1;
//	int delta = 0;
//	Mat grad_xLeft, grad_yLeft;
//	Mat grad_xRight, grad_yRight;
//
//	Mat abs_grad_xLeft, abs_grad_yLeft;
//	Mat abs_grad_xRight, abs_grad_yRight;
//
//	/// Gradient X
//	Sobel(leftSectionHost, grad_xLeft, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT);
//	convertScaleAbs(grad_xLeft, abs_grad_xLeft);
//
//	Sobel(rightSectionHost, grad_xRight, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT);
//	convertScaleAbs(grad_xRight, abs_grad_xRight);
//
//	/// Gradient Y
//	Sobel(leftSectionHost, grad_yLeft, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT);
//	convertScaleAbs(grad_yLeft, abs_grad_yLeft);
//
//	Sobel(rightSectionHost, grad_yRight, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT);
//	convertScaleAbs(grad_yRight, abs_grad_yRight);
//
//	/// Total Gradient (approximate)
//	addWeighted(abs_grad_xLeft, 0.5, abs_grad_yLeft, 0.5, 0, leftSectionHost);
//	addWeighted(abs_grad_xRight, 0.5, abs_grad_yRight, 0.5, 0, rightSectionHost);
//
//	leftDevice.upload(leftSectionHost);
//	rightDevice.upload(rightSectionHost);
//}
//
///*********************************************************
//Launch kernel for all pixels to calculate Matching Cost
//**********************************************************/
//void SGM::PixelwiseCostCalculation()
//{
//	// Configuring Grid
//	numberOfThreads.x = paramsHost->disparityRange;
//	numberofBlocks.x = paramsHost->imageSectionHeight;
//	numberofBlocks.y = paramsHost->imageSectionWidth;
//
//	clock_t begin = clock();
//
//	PixelwiseCostCalculationKernel << < numberofBlocks, numberOfThreads >> > (thrust::raw_pointer_cast(cost.data()), leftDevice, rightDevice, paramsDevice);
//	checkCudaError(cudaDeviceSynchronize(), "Pixelwise device synchronize");
//
//	clock_t end = clock();
//	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
//	cout << "Time taken: " << elapsed_secs << endl;
//}
//
//__global__ void PixelwiseCostCalculationKernel(float *costDevice, cuda::PtrStepSz<unsigned char> leftMat, cuda::PtrStepSz<unsigned char> rightMat, algorithmParameters *paramsDevice)
//{
//	short int blockRow, blockCol;
//	float sad = 0;
//
//	int row = blockIdx.x,
//		col = blockIdx.y,
//		d = threadIdx.x;
//
//	blockRow = -matchingCostBlockSize / 2;
//#pragma unroll
//	while (blockRow <= matchingCostBlockSize / 2)
//	{
//		blockCol = -matchingCostBlockSize / 2;
//#pragma unroll
//		while (blockCol <= matchingCostBlockSize / 2)
//		{
//			if (isPixelWithinBounds(row + blockRow, col + blockCol, paramsDevice) && isPixelWithinBounds(row + blockRow, col + blockCol - d, paramsDevice))
//				sad += abs((float)(leftMat.ptr(row + blockRow)[col + blockCol]) - (float)(rightMat.ptr(row + blockRow)[col + blockCol - d]));
//			else if (isPixelWithinBounds(row + blockRow, col + blockCol, paramsDevice))
//				sad += (float)(leftMat.ptr(row + blockRow)[col + blockCol]);
//			++blockCol;
//		}
//		++blockRow;
//	}
//
//	size_t elementIndex = (row * paramsDevice->nColsXDisparityRange);
//	elementIndex += (col * paramsDevice->disparityRange);
//	elementIndex += d;
//	costDevice[elementIndex] = sad;
//}
//
//void SGM::AggregateCosts()
//{
//	// Below are various flags to make kernel launch easier and use just two kernels for all the paths
//
//	int rowNotNeeded = -1,	// If rows are parallelized, then rows need not be sent to the kernel
//		colNotNeeded = -1;	// Similarly, if columns are parallelized, then columns need not be sent to the kernel
//
//	int	rowDifferenceNegative = -1,	// These specify the previous path
//		rowDifferenceZero = 0,
//		rowDifferencePositive = 1,
//
//		colDifferenceNegative = -1,
//		colDifferenceZero = 0,
//		colDifferencePositive = 1;
//
//	bool rowAndColToBeSubtracted = true;	// Required for the diagonal slice logic
//
//	numberOfThreads.x = paramsHost->disparityRange;
//
//	checkCudaError(cudaDeviceSynchronize(), "Aggregate Costs Device Synchronize");
//
//	dim3 numberofBlocksForAllRows, numberofBlocksForAllColumns, numberofBlocksForAllSlices;
//
//	// When the rows are parallelized
//	numberofBlocksForAllRows.x = paramsHost->imageSectionHeight;
//	numberofBlocksForAllRows.y = 1;
//	numberofBlocksForAllRows.z = 1;
//
//	// When the columns are parallelized
//	numberofBlocksForAllColumns.x = 1;
//	numberofBlocksForAllColumns.y = paramsHost->imageSectionWidth;
//	numberofBlocksForAllColumns.z = 1;
//
//	// When the diagonal slices are parallelized
//	numberofBlocksForAllSlices.x = (paramsHost->imageSectionHeight < paramsHost->imageSectionWidth) ? paramsHost->imageSectionHeight : paramsHost->imageSectionWidth;
//	numberofBlocksForAllSlices.y = 1;
//	numberofBlocksForAllSlices.z = 1;
//
//	if (currentVariant == AllPaths)
//	{
//		/* All paths run sequentially */
//
//		int row, col, slice, numberOfSlices = paramsHost->imageSectionWidth + paramsHost->imageSectionHeight - 1;
//
//		// Path 1: 0, -1
//		for (col = 0; col < paramsHost->imageSectionWidth; ++col)
//			AggregateCostsNonDiagonalKernel << < numberofBlocksForAllRows, numberOfThreads >> >(rowNotNeeded, col, rowDifferenceZero, colDifferenceNegative, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath1.data()), paramsDevice);
//
//		// Path 2: -1, -1
//		for (slice = 0; slice < numberOfSlices; ++slice)
//			AggregateCostsDiagonalKernel << < numberofBlocksForAllSlices, numberOfThreads >> >(slice, !rowAndColToBeSubtracted, rowDifferenceNegative, colDifferenceNegative, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath1.data()), paramsDevice);
//
//		// Path 3: -1, 0
//		for (row = 0; row < paramsHost->imageSectionHeight; ++row)
//			AggregateCostsNonDiagonalKernel << < numberofBlocksForAllColumns, numberOfThreads >> >(row, colNotNeeded, rowDifferenceNegative, colDifferenceZero, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath1.data()), paramsDevice);
//
//		// Path 4: -1, 1
//		for (slice = paramsHost->imageSectionWidth - 1; slice > -paramsHost->imageSectionHeight; --slice)
//			AggregateCostsDiagonalKernel << < numberofBlocksForAllSlices, numberOfThreads >> >(slice, rowAndColToBeSubtracted, rowDifferenceNegative, colDifferencePositive, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath1.data()), paramsDevice);
//
//		// Path 5: 0, 1
//		for (col = paramsHost->imageSectionWidth - 1; col >= 0; --col)
//			AggregateCostsNonDiagonalKernel << < numberofBlocksForAllRows, numberOfThreads >> >(rowNotNeeded, col, rowDifferenceZero, colDifferencePositive, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath1.data()), paramsDevice);
//
//		// Path 6: 1, 1
//		for (slice = numberOfSlices - 1; slice >= 0; --slice)
//			AggregateCostsDiagonalKernel << < numberofBlocksForAllSlices, numberOfThreads >> >(slice, !rowAndColToBeSubtracted, rowDifferencePositive, colDifferencePositive, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath1.data()), paramsDevice);
//
//		//// Path 7: 1, 0
//		for (row = paramsHost->imageSectionHeight - 1; row >= 0; --row)
//			AggregateCostsNonDiagonalKernel << < numberofBlocksForAllColumns, numberOfThreads >> >(row, colNotNeeded, rowDifferencePositive, colDifferenceZero, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath1.data()), paramsDevice);
//
//		// Path 8: 1, -1
//		for (slice = -(paramsHost->imageSectionHeight - 1); slice < paramsHost->imageSectionWidth; ++slice)
//			AggregateCostsDiagonalKernel << < numberofBlocksForAllSlices, numberOfThreads >> >(slice, rowAndColToBeSubtracted, rowDifferencePositive, colDifferencePositive, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath1.data()), paramsDevice);
//
//		checkCudaError(cudaDeviceSynchronize(), "Aggregation All Paths Synchronization");
//	}
//	else if (currentVariant == StreamedAllPaths)
//	{
//		/* Ungodly concurrent while loops
//		These loops make sure that the paths are executed in the intended order in the right
//		streams:
//
//		Stream 1 | Stream 2
//		--------------------
//		Path 1   | Path 5
//		Path 2   | Path 6
//		Path 3   | Path 7
//		Path 4   | Path 8    */
//
//		int rowPath3, rowPath7, colPath1, colPath5, slicePath2, slicePath4, slicePath6, slicePath8, numberOfSlices = paramsHost->imageSectionWidth + paramsHost->imageSectionHeight - 1;
//
//		colPath1 = 0; colPath5 = paramsHost->imageSectionWidth - 1;
//		bool Stream1Complete = false, Stream2Complete = false;
//		do {
//			// Path 1: 0, -1
//			if (colPath1 < paramsHost->imageSectionWidth)
//				AggregateCostsNonDiagonalKernel << < numberofBlocksForAllRows, numberOfThreads, 0, Stream1 >> >(rowNotNeeded, colPath1++, rowDifferenceZero, colDifferenceNegative, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath1.data()), paramsDevice);
//			else
//				Stream1Complete = true;
//
//			// Path 5: 0, 1
//			if (colPath5 >= 0)
//				AggregateCostsNonDiagonalKernel << < numberofBlocksForAllRows, numberOfThreads, 0, Stream2 >> >(rowNotNeeded, colPath5--, rowDifferenceZero, colDifferencePositive, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath2.data()), paramsDevice);
//			else
//				Stream2Complete = true;
//		} while (!Stream1Complete || !Stream2Complete);
//
//		slicePath2 = 0; slicePath6 = numberOfSlices - 1;
//		Stream1Complete = false, Stream2Complete = false;
//		do {
//			// Path 2: -1, -1
//			if (slicePath2 < numberOfSlices)
//				AggregateCostsDiagonalKernel << < numberofBlocksForAllSlices, numberOfThreads, 0, Stream1 >> >(slicePath2++, !rowAndColToBeSubtracted, rowDifferenceNegative, colDifferenceNegative, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath1.data()), paramsDevice);
//			else
//				Stream1Complete = true;
//
//			// Path 6: 1, 1
//			if (slicePath6 >= 0)
//				AggregateCostsDiagonalKernel << < numberofBlocksForAllSlices, numberOfThreads, 0, Stream2 >> >(slicePath6--, !rowAndColToBeSubtracted, rowDifferencePositive, colDifferencePositive, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath2.data()), paramsDevice);
//			else
//				Stream2Complete = true;
//
//		} while (!Stream1Complete || !Stream2Complete);
//
//		rowPath3 = 0; rowPath7 = paramsHost->imageSectionHeight - 1;
//		Stream1Complete = false; Stream2Complete = false;
//		do
//		{
//			// Path 3: -1, 0
//			if (rowPath3 < paramsHost->imageSectionHeight)
//				AggregateCostsNonDiagonalKernel << < numberofBlocksForAllColumns, numberOfThreads, 0, Stream1 >> >(rowPath3++, colNotNeeded, rowDifferenceNegative, colDifferenceZero, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath1.data()), paramsDevice);
//			else
//				Stream1Complete = true;
//
//			// Path 7: 1, 0
//			if (rowPath7 >= 0)
//				AggregateCostsNonDiagonalKernel << < numberofBlocksForAllColumns, numberOfThreads, 0, Stream2 >> >(rowPath7--, colNotNeeded, rowDifferencePositive, colDifferenceZero, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath2.data()), paramsDevice);
//			else
//				Stream2Complete = true;
//		} while (!Stream1Complete || !Stream2Complete);
//
//		slicePath4 = paramsHost->imageSectionWidth - 1; slicePath8 = -(paramsHost->imageSectionHeight - 1);
//		Stream1Complete = false; Stream2Complete = false;
//		do
//		{
//			// Path 4: -1, 1
//			if (slicePath4 > -paramsHost->imageSectionHeight)
//				AggregateCostsDiagonalKernel << < numberofBlocksForAllSlices, numberOfThreads, 0, Stream1 >> >(slicePath4--, rowAndColToBeSubtracted, rowDifferenceNegative, colDifferencePositive, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath1.data()), paramsDevice);
//			else
//				Stream1Complete = true;
//
//			// Path 8: 1, -1
//			if (slicePath8 < paramsHost->imageSectionWidth)
//				AggregateCostsDiagonalKernel << < numberofBlocksForAllSlices, numberOfThreads, 0, Stream2 >> >(slicePath8++, rowAndColToBeSubtracted, rowDifferencePositive, colDifferencePositive, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath2.data()), paramsDevice);
//			else
//				Stream2Complete = true;
//		} while (!Stream1Complete || !Stream2Complete);
//
//		checkCudaError(cudaStreamSynchronize(Stream1), "Stream 1 Aggregation Synchronization");
//		checkCudaError(cudaStreamSynchronize(Stream2), "Stream 2 Aggregation Synchronization");
//		checkCudaError(cudaDeviceSynchronize(), "Stream StreamedAllPaths Device Synchronization");
//	}
//	else if (currentVariant == StreamedHalfPaths)
//	{
//		/* Ungodly concurrent while loops
//		These loops make sure that the paths are executed in the intended order in the right
//		streams:
//
//		Stream 1 | Stream 2 | Stream 3 | Stream 4
//		-----------------------------------------
//		Path 1   | Path 3   | Path 5   | Path 7   */
//
//		int colPath1 = 0, rowPath3 = 0, colPath5 = paramsHost->imageSectionWidth - 1, rowPath7 = paramsHost->imageSectionHeight - 1;
//		bool Stream1Complete = false, Stream2Complete = false, Stream3Complete = false, Stream4Complete = false;
//		do {
//			// Path 1: 0, -1
//			if (colPath1 < paramsHost->imageSectionWidth)
//				AggregateCostsNonDiagonalKernel << < numberofBlocksForAllRows, numberOfThreads, 0, Stream1 >> >(rowNotNeeded, colPath1++, rowDifferenceZero, colDifferenceNegative, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath1.data()), paramsDevice);
//			else
//				Stream1Complete = true;
//
//			// Path 3: -1, 0
//			if (rowPath3 < paramsHost->imageSectionHeight)
//				AggregateCostsNonDiagonalKernel << < numberofBlocksForAllColumns, numberOfThreads, 0, Stream2 >> > (rowPath3++, colNotNeeded, rowDifferenceNegative, colDifferenceZero, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath2.data()), paramsDevice);
//			else
//				Stream2Complete = true;
//
//			// Path 5: 0, 1
//			if (colPath5 >= 0)
//				AggregateCostsNonDiagonalKernel << < numberofBlocksForAllRows, numberOfThreads, 0, Stream3 >> >(rowNotNeeded, colPath5--, rowDifferenceZero, colDifferencePositive, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath3.data()), paramsDevice);
//			else
//				Stream3Complete = true;
//
//			// Path 7: 1, 0
//			if (rowPath7 >= 0)
//				AggregateCostsNonDiagonalKernel << < numberofBlocksForAllColumns, numberOfThreads, 0, Stream4 >> >(rowPath7--, colNotNeeded, rowDifferencePositive, colDifferenceZero, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath4.data()), paramsDevice);
//			else
//				Stream4Complete = true;
//
//		} while (!Stream1Complete || !Stream2Complete || !Stream3Complete || !Stream4Complete);
//
//		//int numberOfSlices = paramsHost->imageSectionWidth + paramsHost->imageSectionHeight - 1;
//		//
//		//int slicePath2 = 0, slicePath4 = paramsHost->imageSectionWidth - 1, slicePath6 = numberOfSlices - 1, slicePath8 = -(paramsHost->imageSectionHeight - 1);
//		//Stream1Complete = false; Stream2Complete = false; Stream3Complete = false; Stream4Complete = false;
//
//		//do {
//		//	// Path 2: -1, -1
//		//	if (slicePath2 < numberOfSlices)
//		//		AggregateCostsDiagonalKernel << < numberofBlocksForAllSlices, numberOfThreads, 0, Stream1 >> >(slicePath2++, !rowAndColToBeSubtracted, rowDifferenceNegative, colDifferenceNegative, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath1.data()), paramsDevice);
//		//	else
//		//		Stream1Complete = true;
//
//		//	// Path 4: -1, 1
//		//	if (slicePath4 > -paramsHost->imageSectionHeight)
//		//		AggregateCostsDiagonalKernel << < numberofBlocksForAllSlices, numberOfThreads, 0, Stream2 >> >(slicePath4--, rowAndColToBeSubtracted, rowDifferenceNegative, colDifferencePositive, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath2.data()), paramsDevice);
//		//	else
//		//		Stream2Complete = true;
//
//		//	// Path 6: 1, 1
//		//	if (slicePath6 >= 0)
//		//		AggregateCostsDiagonalKernel << < numberofBlocksForAllSlices, numberOfThreads, 0, Stream3 >> >(slicePath6--, !rowAndColToBeSubtracted, rowDifferencePositive, colDifferencePositive, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath3.data()), paramsDevice);
//		//	else
//		//		Stream3Complete = true;
//
//		//	// Path 8: 1, -1
//		//	if (slicePath8 < paramsHost->imageSectionWidth)
//		//		AggregateCostsDiagonalKernel << < numberofBlocksForAllSlices, numberOfThreads, 0, Stream4 >> >(slicePath8++, rowAndColToBeSubtracted, rowDifferencePositive, colDifferencePositive, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath4.data()), paramsDevice);
//		//	else
//		//		Stream4Complete = true;
//
//		//} while (!Stream1Complete || !Stream2Complete || !Stream3Complete || !Stream4Complete);
//
//		checkCudaError(cudaStreamSynchronize(Stream1), "Stream 1 Aggregation Synchronization");
//		checkCudaError(cudaStreamSynchronize(Stream2), "Stream 2 Aggregation Synchronization");
//		checkCudaError(cudaStreamSynchronize(Stream3), "Stream 3 Aggregation Synchronization");
//		checkCudaError(cudaStreamSynchronize(Stream4), "Stream 4 Aggregation Synchronization");
//		checkCudaError(cudaDeviceSynchronize(), "Stream StreamedHalfPaths Device Synchronization");
//	}
//}
//
//__global__ void AggregateCostsNonDiagonalKernel(int row, int col, int rowDifference, int colDifference, __int16 *costDevice, __int16 *aggregatedCostsDevice, algorithmParameters *paramsDevice)
//{
//	// If the row or column are supplied or not
//	row = row == -1 ? blockIdx.x : row;
//	col = col == -1 ? blockIdx.y : col;
//
//	int d = threadIdx.x;
//	size_t elementIndex = (row * paramsDevice->nColsXDisparityRange) + (col * paramsDevice->disparityRange) + d;
//
//	aggregatedCostsDevice[elementIndex] += costDevice[elementIndex];
//
//	if (isPixelWithinBounds(row + rowDifference, col + colDifference, paramsDevice))
//		aggregatedCostsDevice[elementIndex] += AggregateCostsPreviousPathKernel(((row + rowDifference) * paramsDevice->nColsXDisparityRange) + ((col + colDifference) * paramsDevice->disparityRange), d, aggregatedCostsDevice, paramsDevice);
//}
//
//__global__ void AggregateCostsDiagonalKernel(int slice, bool rowAndColToBeSubtracted, int rowDifference, int colDifference, __int16 *costDevice, __int16 *aggregatedCostsDevice, algorithmParameters *paramsDevice)
//{
//	int sliceBlockSize = (paramsDevice->imageSectionHeight < paramsDevice->imageSectionWidth) ? paramsDevice->imageSectionHeight : paramsDevice->imageSectionWidth;
//
//	int row = blockIdx.x, col = 0;
//
//	while (row >= 0 && row < paramsDevice->imageSectionHeight)
//	{
//		if (rowAndColToBeSubtracted)
//			col = row - slice;
//		else if (!rowAndColToBeSubtracted)
//			col = slice - row;
//
//		if (col < 0 || col >= paramsDevice->imageSectionWidth)
//			return;
//
//		if (!isPixelWithinBounds(row, col, paramsDevice))
//			return;
//
//		if ((!rowAndColToBeSubtracted && (row + col == slice)) || (rowAndColToBeSubtracted && (row - col == slice)))
//		{
//			int d = threadIdx.x;
//			size_t elementIndex = (row * paramsDevice->nColsXDisparityRange) + (col * paramsDevice->disparityRange) + d;
//
//			aggregatedCostsDevice[elementIndex] += costDevice[elementIndex];
//
//			if (isPixelWithinBounds(row + rowDifference, col + colDifference, paramsDevice))
//				aggregatedCostsDevice[elementIndex] += AggregateCostsPreviousPathKernel(((row + rowDifference) * paramsDevice->nColsXDisparityRange) + ((col + colDifference) * paramsDevice->disparityRange), d, aggregatedCostsDevice, paramsDevice);
//		}
//
//		row += sliceBlockSize;
//	}
//}
//
//__device__ float AggregateCostsPreviousPathKernel(size_t pixelIndex, int currentDisparity, float *aggregatedCostsDevice, algorithmParameters *paramsDevice)
//{
//	float c1 = aggregatedCostsDevice[pixelIndex + currentDisparity],
//		c2 = (currentDisparity - 1) >= 0 ? aggregatedCostsDevice[pixelIndex + currentDisparity - 1] + paramsDevice->p1 : INT32_MAX,
//		c3 = (currentDisparity + 1) < paramsDevice->disparityRange ? aggregatedCostsDevice[pixelIndex + currentDisparity + 1] + paramsDevice->p1 : INT32_MAX,
//		c4;
//
//	c4 = aggregatedCostsDevice[pixelIndex];
//	for (int i = 1; i < paramsDevice->disparityRange; ++i)
//		if (c4 > aggregatedCostsDevice[pixelIndex + i])
//			c4 = aggregatedCostsDevice[pixelIndex + i];
//
//	return MIN(
//		MIN(c1, c2),
//		MIN(c3, c4 + paramsDevice->p2)
//	) - c4;
//}
//
//void SGM::DisparityCalculation()
//{
//	numberofBlocks.x = paramsHost->imageSectionHeight;
//	numberofBlocks.y = paramsHost->imageSectionWidth;
//	numberOfThreads.x = 1;
//	numberOfThreads.y = 1;
//
//	checkCudaError(cudaDeviceSynchronize(), "Disparity Calculation Device Synchronize");
//	switch (currentVariant)
//	{
//	case AllPaths:
//		DisparityCalculationKernelAllPaths << < numberofBlocks, numberOfThreads >> > (thrust::raw_pointer_cast(aggregatedCostsPath1.data()), disparityDevice, paramsDevice);
//		break;
//	case StreamedAllPaths:
//		DisparityCalculationKernelStreamedAllPaths << < numberofBlocks, numberOfThreads >> > (thrust::raw_pointer_cast(aggregatedCostsPath1.data()), thrust::raw_pointer_cast(aggregatedCostsPath2.data()), disparityDevice, paramsDevice);
//		break;
//	case StreamedHalfPaths:
//		DisparityCalculationKernelStreamedHalfPaths << < numberofBlocks, numberOfThreads >> > (thrust::raw_pointer_cast(aggregatedCostsPath1.data()), thrust::raw_pointer_cast(aggregatedCostsPath2.data()), thrust::raw_pointer_cast(aggregatedCostsPath3.data()), thrust::raw_pointer_cast(aggregatedCostsPath4.data()), disparityDevice, paramsDevice);
//		break;
//	}
//	checkCudaError(cudaDeviceSynchronize(), "Disparity Calculation Complete");
//}
//
//__global__ void DisparityCalculationKernelAllPaths(float *aggregatedCostPath1Device, cv::cuda::PtrStepSz<short int> disparityMat, algorithmParameters *paramsDevice)
//{
//	int row = blockIdx.x,
//		col = blockIdx.y;
//	float maxInt16 = 32767.0;
//
//	int pixelIndex = (row * paramsDevice->nColsXDisparityRange) + (col * paramsDevice->disparityRange);
//
//	int minD = 0;
//	for (int d = 1; d < paramsDevice->disparityRange; ++d)
//		if (aggregatedCostPath1Device[pixelIndex + minD] > aggregatedCostPath1Device[pixelIndex + d])
//			minD = d;
//	disparityMat.ptr(row)[col] = (short int)((minD * maxInt16) / paramsDevice->disparityRange);
//}
//
//__global__ void DisparityCalculationKernelStreamedAllPaths(float *aggregatedCostPath1Device, float *aggregatedCostPath2Device, cv::cuda::PtrStepSz<short int> disparityMat, algorithmParameters *paramsDevice)
//{
//	int row = blockIdx.x,
//		col = blockIdx.y;
//	float maxInt16 = 32767.0;
//
//	size_t pixelIndex = (row * paramsDevice->nColsXDisparityRange) + (col * paramsDevice->disparityRange);
//
//	int minD = 0, minD1 = 0, minD2 = 0;
//	for (int d = 1; d < paramsDevice->disparityRange; ++d)
//	{
//		if (aggregatedCostPath1Device[pixelIndex + minD1] > aggregatedCostPath1Device[pixelIndex + d])
//			minD1 = d;
//		if (aggregatedCostPath2Device[pixelIndex + minD2] > aggregatedCostPath2Device[pixelIndex + d])
//			minD2 = d;
//	}
//	minD = aggregatedCostPath1Device[pixelIndex + minD1] > aggregatedCostPath2Device[pixelIndex + minD2] ? minD2 : minD1;
//	disparityMat.ptr(row)[col] = (short int)((minD * maxInt16) / paramsDevice->disparityRange);
//}
//
//__global__ void DisparityCalculationKernelStreamedHalfPaths(float *aggregatedCostPath1Device, float *aggregatedCostPath2Device, float *aggregatedCostPath3Device, float *aggregatedCostPath4Device, cv::cuda::PtrStepSz<short int> disparityMat, algorithmParameters *paramsDevice)
//{
//	int row = blockIdx.x,
//		col = blockIdx.y;
//	float maxInt16 = 32767.0;
//
//	size_t pixelIndex = (row * paramsDevice->nColsXDisparityRange) + (col * paramsDevice->disparityRange);
//
//	int minD = 0, minD1 = 0, minD2 = 0, minD3 = 0, minD4 = 0;
//	for (int d = 1; d < paramsDevice->disparityRange; ++d)
//	{
//		if (aggregatedCostPath1Device[pixelIndex + minD1] > aggregatedCostPath1Device[pixelIndex + d])
//			minD1 = d;
//		if (aggregatedCostPath2Device[pixelIndex + minD2] > aggregatedCostPath2Device[pixelIndex + d])
//			minD2 = d;
//		if (aggregatedCostPath3Device[pixelIndex + minD3] > aggregatedCostPath3Device[pixelIndex + d])
//			minD3 = d;
//		if (aggregatedCostPath4Device[pixelIndex + minD4] > aggregatedCostPath4Device[pixelIndex + d])
//			minD4 = d;
//	}
//
//	int minC1, minC3;
//	if (aggregatedCostPath1Device[pixelIndex + minD1] < aggregatedCostPath2Device[pixelIndex + minD2])
//		minC1 = aggregatedCostPath1Device[pixelIndex + minD1];
//	else {
//		minC1 = aggregatedCostPath2Device[pixelIndex + minD2];
//		minD1 = minD2;
//	}
//	if (aggregatedCostPath3Device[pixelIndex + minD3] < aggregatedCostPath4Device[pixelIndex + minD4])
//		minC3 = aggregatedCostPath3Device[pixelIndex + minD3];
//	else {
//		minC3 = aggregatedCostPath4Device[pixelIndex + minD4];
//		minD3 = minD4;
//	}
//	minD = minC1 < minC3 ? minD1 : minD3;
//	disparityMat.ptr(row)[col] = (short int)((minD * maxInt16) / paramsDevice->disparityRange);
//}
//
//void SGM::StitchImageSections()
//{
//	checkCudaError(cudaDeviceSynchronize(), "Generate Map Device Synchronize");
//	disparityDevice.download(disparitySectionHost);
//	disparitySectionHost.copyTo(disparityHost(Rect(paramsHost->imageSectionStartX, paramsHost->imageSectionStartY, paramsHost->imageSectionWidth, paramsHost->imageSectionHeight)));
//}
//
//void SGM::ResetArrays()
//{
//	thrust::fill(cost.begin(), cost.end(), 0);
//	switch (currentVariant)
//	{
//	case AllPaths:
//		thrust::fill(aggregatedCostsPath1.begin(), aggregatedCostsPath1.end(), 0);
//		break;
//	case StreamedAllPaths:
//		thrust::fill(aggregatedCostsPath1.begin(), aggregatedCostsPath1.end(), 0);
//		thrust::fill(aggregatedCostsPath2.begin(), aggregatedCostsPath2.end(), 0);
//		break;
//	case StreamedHalfPaths:
//		thrust::fill(aggregatedCostsPath1.begin(), aggregatedCostsPath1.end(), 0);
//		thrust::fill(aggregatedCostsPath2.begin(), aggregatedCostsPath2.end(), 0);
//		thrust::fill(aggregatedCostsPath3.begin(), aggregatedCostsPath3.end(), 0);
//		thrust::fill(aggregatedCostsPath4.begin(), aggregatedCostsPath4.end(), 0);
//		break;
//	}
//	leftDevice.release();
//	rightDevice.release();
//}
//
//void SGM::PostProcessAndGenerateMap()
//{
//	// Median Filter
//	//medianBlur(disparityHost, disparityHost, 7);
//
//	int specklePaintoverValue = 0,
//		maxSpecklesize = 150,
//		maxNeighborDifference = 16;
//	filterSpeckles(disparityHost, specklePaintoverValue, maxSpecklesize, maxNeighborDifference);
//
//	//dilate(disparityHost, disparityHost, Mat::ones(3, 3, CV_8U), Point(-1, -1), 3);
//
//	// Normalizing to greyscale
//	normalize(disparityHost, disparityHost, 1, 255, NORM_MINMAX, CV_8UC1);
//
//	// Generate Map
//	imwrite(disparityFilePath, disparityHost);
//
//	if (groundTruth.data)
//		calculateError();
//}
//
//SGM::~SGM()
//{
//	checkCudaError(cudaDeviceSynchronize(), "Deallocation");
//
//	leftDevice.release();
//	rightDevice.release();
//	disparityDevice.release();
//
//	checkCudaError(cudaFree(paramsDevice), "Freeing parameter struct");
//
//	cost.clear(); cost.shrink_to_fit();
//	switch (currentVariant)
//	{
//	case AllPaths:
//		aggregatedCostsPath1.clear(); aggregatedCostsPath1.shrink_to_fit();
//		break;
//	case StreamedAllPaths:
//		aggregatedCostsPath1.clear(); aggregatedCostsPath1.shrink_to_fit();
//		aggregatedCostsPath2.clear(); aggregatedCostsPath2.shrink_to_fit();
//		break;
//	case StreamedHalfPaths:
//		aggregatedCostsPath1.clear(); aggregatedCostsPath1.shrink_to_fit();
//		aggregatedCostsPath2.clear(); aggregatedCostsPath2.shrink_to_fit();
//		aggregatedCostsPath3.clear(); aggregatedCostsPath3.shrink_to_fit();
//		aggregatedCostsPath4.clear(); aggregatedCostsPath4.shrink_to_fit();
//		break;
//	}
//}
//
//std::string SGM::getVariantName()
//{
//	switch (currentVariant)
//	{
//	case AllPaths:
//		return "AllPaths";
//	case StreamedAllPaths:
//		return "StreamedAllPaths";
//	case StreamedHalfPaths:
//		return "StreamedHalfPaths";
//	}
//}
//
//void SGM::checkCudaError(cudaError_t cudaStatus, string message)
//{
//	if (cudaStatus != cudaSuccess)
//	{
//		cout << "Cuda Error in: " << message << " with error code: " << cudaGetErrorString(cudaStatus) << endl;
//		exit(EXIT_FAILURE);
//	}
//}
//
//void SGM::calculateError()
//{
//	// The ground truth tends to lighter than the depth map, so it is multiplied by a scalar to come be in the same
//	// light range as the depth map.
//
//	float correction = 1, finalError = 100, finalAverageError = 100, errorThreshold = 4.0, correctCorrection = 0;
//
//	for (correction = 0; correction <= 1; correction += 0.2)
//	{
//		float error = 0, averageError = 0;
//		Mat changedGroundTruth = correction * groundTruth;
//
//		float pixelDifference = 0;
//		for (int row = 0; row < disparityHost.rows; ++row)
//			for (int col = 0; col < disparityHost.cols; ++col)
//			{
//				if (mask.data)
//					if ((int)mask.ptr<unsigned char>(row)[col] != 255)
//						continue;
//
//				pixelDifference = abs((int)disparityHost.ptr<unsigned char>(row)[col] - (int)changedGroundTruth.ptr<unsigned char>(row)[col]);
//				if (abs((int)disparityHost.ptr<unsigned char>(row)[col] - (int)changedGroundTruth.ptr<unsigned char>(row)[col]) > errorThreshold)
//					error++;
//				averageError += abs((int)disparityHost.ptr<unsigned char>(row)[col] - (int)changedGroundTruth.ptr<unsigned char>(row)[col]);
//			}
//		error = error * 100 / (disparityHost.rows * disparityHost.cols);
//		averageError /= (disparityHost.rows * disparityHost.cols);
//
//		if (finalError > error)
//		{
//			finalError = error;
//			finalAverageError = averageError;
//			correctCorrection = correction;
//		}
//
//	}
//	cout << "Correction applied to ground truth: " << correctCorrection << endl;
//	cout << "Error: " << finalError << endl;
//	cout << "Avg Error: " << finalAverageError << endl << endl;
//}
//
