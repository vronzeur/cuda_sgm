#include "sgm.cuh"
#include <boost\program_options.hpp>
#include <boost\filesystem.hpp>

namespace po = boost::program_options;
namespace fs = boost::filesystem;

int main(int ac, char* av[])
{
	std::string leftFilepathString, rightFilepathString, groundTruthFilepathString, maskFilepathString;
	fs::path leftPath, rightPath, grountTruthPath, maskPath;

	po::options_description desc("Allowed Description");
	desc.add_options()
		("help,h", "produce help message")
		("left,l", po::value<std::string>(&leftFilepathString), "left image file path")
		("right,r", po::value<std::string>(&rightFilepathString), "right image file path")
		("groundtruth,g", po::value<std::string>(&groundTruthFilepathString), "ground truth image file path")
		("mask,m", po::value<std::string>(&maskFilepathString), "ground truth image file path")
		;

	po::variables_map vm;
	po::store(po::parse_command_line(ac, av, desc), vm);
	po::notify(vm);

	if (vm.count("help"))
		std::cout << desc << std::endl;

	if (vm.count("left"))
		leftPath = fs::path(leftFilepathString);

	if (vm.count("right"))
		rightPath = fs::path(rightFilepathString);

	if (vm.count("groundtruth"))
		grountTruthPath = fs::path(groundTruthFilepathString);

	if (vm.count("mask"))
		maskPath = fs::path(maskFilepathString);

	if (ac < 3 || (leftFilepathString.length() == 0) || (rightFilepathString.length() == 0))
	{
		std::cout << "Missing left and/or right file paths. Use -h/--help to see usage options" << std::endl;
		return -1;
	}

	std::string branchPath = leftPath.branch_path().string().length() > 1 ? leftPath.branch_path().string() + "\\" : "";

	SGM *s = new SGM(leftFilepathString, rightFilepathString, groundTruthFilepathString, maskFilepathString, branchPath);
	s->calculateDisparity();
	delete s;

	if (cudaDeviceReset() != cudaSuccess) std::cout << "Not Reset" << std::endl;

	return 0;
}