/********************************************************************************
SGM GPU Header
Contains the global variables and functions for the functioning of the algorithm
********************************************************************************/

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda.h>
#include <thrust/device_vector.h>
#include <thrust/device_malloc.h>
#include <thrust/device_free.h>
#include <thrust/extrema.h>

//#include <cub\cub.cuh>

#include <stdio.h>
#include <iostream>
#include <math.h>

#include <opencv2\core.hpp>
#include <opencv2\core\cuda.hpp>
#include <opencv2\highgui.hpp>
#include <opencv2\imgproc.hpp>
#include <opencv2\calib3d.hpp>

#define matchingCostBlockSize 3
#define cudaDeviceToSelect 0

struct algorithmParameters {
	int disparityRange;

	// The image is split into sections. These variables define these sections
	int imageSectionBeingProcessed,
		imageSectionHeight,
		imageSectionWidth,	// Stays constant since we split the image vertically
		imageSectionStartX,
		imageSectionStartY;

	int columnsDisparityRangeRatio;
	int p1, p2;
	int numberOfImageSections;

	int nColsXDisparityRange; /* Required for indexing an element in the flattened 3D arrays on the device. It is precalculated on the CPU */

	cv::cuda::PtrStepSz<unsigned __int8> leftMat, rightMat;
};

typedef __int16 costType;

class SGM {
	/// Host Variables
	// Image variables
	std::string disparityFilePath;

	cv::Mat leftHost, rightHost, disparityHost;
	cv::cuda::GpuMat leftDevice, rightDevice, disparityDevice;

	cv::Mat leftSectionHost, rightSectionHost, disparitySectionHost;

	cv::Mat groundTruth, mask;

	size_t costArraySize, aggregatedCostArraySize;
	thrust::device_vector<costType> cost;
	thrust::device_vector<costType> aggregatedCostsPath;

	algorithmParameters *paramsHost, *paramsDevice;

	// Streams
	cudaStream_t Stream1, Stream2, Stream3, Stream4;
	dim3 numberOfThreads, numberofBlocks;

	/// Methods
	void SplitImagesIntoSections();
	void ResetArrays();
	void PreProcessAndUploadImagesToDevice();

	void PixelwiseCostCalculation();

	void AggregateCosts();

	void DisparityCalculation();
	void StitchImageSections();

	void PostProcessAndGenerateMap();

	// Helper Functions
	void checkCudaError(cudaError_t cudaStatus, std::string message = "");
public:
	SGM(std::string leftFilepath, std::string rightFilepath, std::string groundTruthFilePath, std::string maskFilePath, std::string subfolderFilepath);
	void calculateDisparity();
	~SGM();

	void calculateError();
};

__global__ void PixelwiseCostCalculationKernel(costType *costDevice, cv::cuda::PtrStepSz<unsigned __int8> leftMat, cv::cuda::PtrStepSz<unsigned __int8> rightMat, algorithmParameters *paramsDevice);

__global__ void AggregateCostsAddCostsKernel(costType *costDevice, costType *aggregatedCostsDevice, algorithmParameters *paramsDevice);
__global__ void AggregateCostsSetPreviousMinimumsNonDiagonalKernel(int row, int col, costType *aggregatedCostsDevice, algorithmParameters *paramsDevice);
__global__ void AggregateCostsSetPreviousMinimumsDiagonalKernel(int slice, bool rowAndColToBeSubtracted, costType *aggregatedCostsDevice, algorithmParameters *paramsDevice);
__global__ void AggregateCostsNonDiagonalKernel(int row, int col, int rowDifference, int colDifference, costType *costDevice, costType *aggregatedCostsDevice, algorithmParameters *paramsDevice, cv::cuda::PtrStepSz<unsigned __int8> leftMat);
__global__ void AggregateCostsDiagonalKernel(int slice, bool rowAndColToBeSubtracted, int rowDifference, int colDifference, costType *costDevice, costType *aggregatedCostsDevice, algorithmParameters *paramsDevice, cv::cuda::PtrStepSz<unsigned __int8> leftMat);
__device__ costType AggregateCostsPreviousPathKernel(size_t pixelIndex, int currentDisparity, costType *aggregatedCostsDevice, algorithmParameters *paramsDevice, float gradientPenalty);
__device__ void AggregateCostsSetMinimumKernel(size_t pixelIndex, int currentDisparity, costType *aggregatedCostsDevice, algorithmParameters *paramsDevice);

__global__ void SADDisparityCalculationKernel(costType *costsDevice, cv::cuda::PtrStepSz<unsigned __int8> disparityMat, algorithmParameters *paramsDevice);
__global__ void DisparityCalculationKernel(costType *aggregatedCostPath1Device, cv::cuda::PtrStepSz<unsigned __int8> disparityMat, algorithmParameters *paramsDevice);

__device__ __forceinline__ bool isPixelWithinBounds(int row, int col, algorithmParameters *paramsDevice)
{
	return (row >= 0) && (row < paramsDevice->imageSectionHeight) && (col >= 0) && (col < paramsDevice->imageSectionWidth);
}