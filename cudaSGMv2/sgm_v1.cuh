///********************************************************************************
//SGM GPU Header
//Contains the global variables and functions for the functioning of the algorithm
//********************************************************************************/
//
//#include "cuda_runtime.h"
//#include "device_launch_parameters.h"
//#include <cuda.h>
//#include <thrust/device_vector.h>
//#include <thrust/device_malloc.h>
//#include <thrust/device_free.h>
//
//#include <stdio.h>
//#include <iostream>
//#include <math.h>
//
//#include <opencv2\core.hpp>
//#include <opencv2\core\cuda.hpp>
//#include <opencv2\highgui.hpp>
//#include <opencv2\imgproc.hpp>
//#include <opencv2\calib3d.hpp>
//
//#define matchingCostBlockSize 3
//#define cudaDeviceToSelect 0
//
//// The value indicates the number of arrays required for each implementation (Cost Array (1) + Aggregated Cost Arrays (2/4)).
//// Useful for calculations in the algorithm as well
//enum AlgorithmVariantToRun {
//	AllPaths = 2,
//	StreamedAllPaths = 3,
//	StreamedHalfPaths = 5
//};
//
//struct algorithmParameters {
//	int disparityRange;
//
//	// The image is split into sections. These variables define these sections
//	int imageSectionBeingProcessed,
//		imageSectionHeight,
//		imageSectionWidth,	// Stays constant since we split the image vertically
//		imageSectionStartX,
//		imageSectionStartY;
//
//	int columnsDisparityRangeRatio;
//	int p1, p2;
//	int numberOfImageSections;
//
//	int nColsXDisparityRange; /* Required for indexing an element in the flattened 3D arrays on the device. It is precalculated on the CPU */
//};
//
//class SGM {
//	/// Host Variables
//	// Image variables
//	std::string disparityFilePath;
//
//	cv::Mat leftHost, rightHost, disparityHost;
//	cv::cuda::GpuMat leftDevice, rightDevice, disparityDevice;
//
//	cv::Mat leftSectionHost, rightSectionHost, disparitySectionHost;
//
//	cv::Mat groundTruth, mask;
//
//	size_t costArraySize, aggregatedCostArraySize;
//	//float *cost;
//	//float *aggregatedCostsPath1, *aggregatedCostsPath2, *aggregatedCostsPath3, *aggregatedCostsPath4;
//	thrust::device_vector<float> cost;
//	thrust::device_vector<float> aggregatedCostsPath1, aggregatedCostsPath2, aggregatedCostsPath3, aggregatedCostsPath4;
//
//	algorithmParameters *paramsHost, *paramsDevice;
//	//thrust::device_ptr<algorithmParameters> paramsDevice;
//
//	AlgorithmVariantToRun currentVariant;
//
//	// Streams
//	cudaStream_t Stream1, Stream2, Stream3, Stream4;
//	dim3 numberOfThreads, numberofBlocks;
//
//	/// Methods
//	void SplitImagesIntoSections();
//	void ResetArrays();
//	void PreProcessAndUploadImagesToDevice();
//
//	void PixelwiseCostCalculation();
//
//	void AggregateCosts();
//
//	void DisparityCalculation();
//	void StitchImageSections();
//
//	void PostProcessAndGenerateMap();
//
//	// Helper Functions
//	void SetGPU();
//	void checkCudaError(cudaError_t cudaStatus, std::string message = "");
//	std::string getVariantName();
//public:
//	SGM(std::string leftFilepath, std::string rightFilepath, std::string groundTruthFilePath, std::string maskFilePath, std::string subfolderFilepath, AlgorithmVariantToRun variant);
//	void calculateDisparity();
//	~SGM();
//
//	void calculateError();
//};
//
//__global__ void PixelwiseCostCalculationKernel(float *costDevice, cv::cuda::PtrStepSz<unsigned char> leftMat, cv::cuda::PtrStepSz<unsigned char> rightMat, algorithmParameters *paramsDevice);
//
//__global__ void AggregateCostsNonDiagonalKernel(int row, int col, int rowDifference, int colDifference, float *costDevice, float *aggregatedCostsDevice, algorithmParameters *paramsDevice);
//__global__ void AggregateCostsDiagonalKernel(int slice, bool rowAndColToBeSubtracted, int rowDifference, int colDifference, float *costDevice, float *aggregatedCostsDevice, algorithmParameters *paramsDevice);
//__device__ float AggregateCostsPreviousPathKernel(size_t pixelIndex, int currentDisparity, float *aggregatedCostsDevice, algorithmParameters *paramsDevice);
//
//__global__ void DisparityCalculationKernelAllPaths(float *aggregatedCostPath1Device, cv::cuda::PtrStepSz<short int> disparityMat, algorithmParameters *paramsDevice);
//__global__ void DisparityCalculationKernelStreamedAllPaths(float *aggregatedCostPath1Device, float *aggregatedCostPath2Device, cv::cuda::PtrStepSz<short int> disparityMat, algorithmParameters *paramsDevice);
//__global__ void DisparityCalculationKernelStreamedHalfPaths(float *aggregatedCostPath1Device, float *aggregatedCostPath2Device, float *aggregatedCostPath3Device, float *aggregatedCostPath4Device, cv::cuda::PtrStepSz<short int> disparityMat, algorithmParameters *paramsDevice);
//
//__device__ __forceinline__ bool isPixelWithinBounds(int row, int col, algorithmParameters *paramsDevice)
//{
//	return (row >= 0) && (row < paramsDevice->imageSectionHeight) && (col >= 0) && (col < paramsDevice->imageSectionWidth);
//}