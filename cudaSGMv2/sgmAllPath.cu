#include "sgm.cuh"
#include <thrust\version.h>

using namespace cv;
using namespace std;

/*********************************************************
calculateDisparity() abstracts the internal working of SGM
**********************************************************/
void SGM::calculateDisparity()
{
	for (paramsHost->imageSectionBeingProcessed = 0; paramsHost->imageSectionBeingProcessed < paramsHost->numberOfImageSections; ++paramsHost->imageSectionBeingProcessed)
	{
		SplitImagesIntoSections();
		PreProcessAndUploadImagesToDevice();
		PixelwiseCostCalculation();

		AggregateCosts();

		DisparityCalculation();
		StitchImageSections();
		ResetArrays();

		cout << "Image Section processed: " << paramsHost->imageSectionBeingProcessed + 1 << " / " << paramsHost->numberOfImageSections << endl;
	}
	PostProcessAndGenerateMap();
}

/*********************************************************
Initialization of all the variables and arrays needed
for the running of the program
**********************************************************/
SGM::SGM(std::string leftFilepath, std::string rightFilepath, std::string groundTruthFilePath, std::string maskFilePath, std::string subfolderFilepath)
{
	// Reading Images
	leftHost = imread(leftFilepath, CV_8UC1);
	rightHost = imread(rightFilepath, CV_8UC1);

	if (!leftHost.data || !rightHost.data)
	{
		cout << "Images could not be read";
		exit(EXIT_FAILURE);
	}

	// Resizing image
	Size FourK(4096, 2160), FullHD(1920, 1080), HDReady(1280, 720), SD(640, 480);
	//resize(leftHost, leftHost, FourK); resize(rightHost, rightHost, FourK); if(groundTruth.data) resize(groundTruth, groundTruth, FourK); if(mask.data)  resize(mask, mask, FourK);
	//resize(leftHost, leftHost, FullHD); resize(rightHost, rightHost, FullHD); if(groundTruth.data) resize(groundTruth, groundTruth, FullHD); if(mask.data)  resize(mask, mask, FullHD);
	//resize(leftHost, leftHost, HDReady); resize(rightHost, rightHost, HDReady); if(groundTruth.data) resize(groundTruth, groundTruth, HDReady); if(mask.data)  resize(mask, mask, HDReady);
	//resize(leftHost, leftHost, SD); resize(rightHost, rightHost, SD); if (groundTruth.data) resize(groundTruth, groundTruth, SD); if (mask.data)  resize(mask, mask, SD);
	//resize(leftHost, leftHost, Size(), 0.25, 0.25); resize(rightHost, rightHost, Size(), 0.25, 0.25);

	disparityFilePath = subfolderFilepath + "disparity" + ".png";
	disparityHost = Mat(leftHost.rows, leftHost.cols, CV_8UC1);

	/// Algorithm Parameters
	// Initializing global algorithm parameters class object
	paramsHost = new algorithmParameters;
	paramsHost->columnsDisparityRangeRatio = 6;

	paramsHost->disparityRange = 128;// leftHost.cols / paramsHost->columnsDisparityRangeRatio;

	paramsHost->imageSectionWidth = leftHost.cols;
	paramsHost->nColsXDisparityRange = paramsHost->imageSectionWidth * paramsHost->disparityRange;	// Used in indexing the flattened 3D device arrays

	paramsHost->p1 = 8 *matchingCostBlockSize * matchingCostBlockSize;
	paramsHost->p2 = 128 *matchingCostBlockSize * matchingCostBlockSize;

	size_t free, total;
	checkCudaError(cudaMemGetInfo(&free, &total), "Getting device properties");

	size_t deviceMemory = free - (350000000.0f);	// Total memory - 350 MB for disparityDevice and other variables
	size_t memoryRequiredForSGM = leftHost.rows * leftHost.cols;
	memoryRequiredForSGM *= paramsHost->disparityRange * sizeof(__int16);

	deviceMemory = (size_t)floorl(deviceMemory / 2);
	paramsHost->numberOfImageSections = floorl(memoryRequiredForSGM % deviceMemory) > 0 ? (int)floorl(memoryRequiredForSGM / deviceMemory) + 1 : (int)floorl(memoryRequiredForSGM / deviceMemory);
	cout << "Image will be split into " << paramsHost->numberOfImageSections << " parts" << endl << endl;

	paramsHost->imageSectionHeight = leftHost.rows / paramsHost->numberOfImageSections;

	// Copying the parameters to the device
	checkCudaError(cudaMalloc(&paramsDevice, sizeof(algorithmParameters)), "Algorithm parameters initialization");

	checkCudaError(cudaMemcpy(&paramsDevice->columnsDisparityRangeRatio, &paramsHost->columnsDisparityRangeRatio, sizeof(int), cudaMemcpyHostToDevice));
	checkCudaError(cudaMemcpy(&paramsDevice->disparityRange, &paramsHost->disparityRange, sizeof(int), cudaMemcpyHostToDevice));
	checkCudaError(cudaMemcpy(&paramsDevice->numberOfImageSections, &paramsHost->numberOfImageSections, sizeof(int), cudaMemcpyHostToDevice));
	checkCudaError(cudaMemcpy(&paramsDevice->imageSectionBeingProcessed, &paramsHost->imageSectionBeingProcessed, sizeof(int), cudaMemcpyHostToDevice));
	checkCudaError(cudaMemcpy(&paramsDevice->imageSectionHeight, &paramsHost->imageSectionHeight, sizeof(int), cudaMemcpyHostToDevice));
	checkCudaError(cudaMemcpy(&paramsDevice->imageSectionWidth, &paramsHost->imageSectionWidth, sizeof(int), cudaMemcpyHostToDevice));
	checkCudaError(cudaMemcpy(&paramsDevice->imageSectionStartX, &paramsHost->imageSectionStartX, sizeof(int), cudaMemcpyHostToDevice));
	checkCudaError(cudaMemcpy(&paramsDevice->imageSectionStartY, &paramsHost->imageSectionStartY, sizeof(int), cudaMemcpyHostToDevice));
	checkCudaError(cudaMemcpy(&paramsDevice->nColsXDisparityRange, &paramsHost->nColsXDisparityRange, sizeof(int), cudaMemcpyHostToDevice));
	checkCudaError(cudaMemcpy(&paramsDevice->p1, &paramsHost->p1, sizeof(int), cudaMemcpyHostToDevice));
	checkCudaError(cudaMemcpy(&paramsDevice->p2, &paramsHost->p2, sizeof(int), cudaMemcpyHostToDevice));

	// Disparity Mat on the Device
	disparityDevice = cuda::GpuMat(paramsHost->imageSectionHeight, paramsHost->imageSectionWidth, CV_8UC1, Scalar(0));

	// Initializing Streams
	checkCudaError(cudaStreamCreate(&Stream1), "Creating Stream 1");
	checkCudaError(cudaStreamCreate(&Stream2), "Creating Stream 2");
	checkCudaError(cudaStreamCreate(&Stream3), "Creating Stream 3");
	checkCudaError(cudaStreamCreate(&Stream4), "Creating Stream 4");

	// Cost Array Sizes
	costArraySize = paramsHost->imageSectionHeight * paramsHost->imageSectionWidth * paramsHost->disparityRange;
	aggregatedCostArraySize = paramsHost->imageSectionHeight * paramsHost->imageSectionWidth * (paramsHost->disparityRange + 1);

	// Cost Arrays
	cost.resize(costArraySize, 0);
	aggregatedCostsPath.resize(aggregatedCostArraySize, 0);
}

/*********************************************************
Launch kernel for all pixels to calculate Matching Cost
**********************************************************/
void SGM::PixelwiseCostCalculation()
{
	// Configuring Grid
	numberOfThreads.x = paramsHost->disparityRange;
	numberofBlocks.x = paramsHost->imageSectionHeight;
	numberofBlocks.y = paramsHost->imageSectionWidth;

	PixelwiseCostCalculationKernel <<< numberofBlocks, numberOfThreads >>> (thrust::raw_pointer_cast(cost.data()), leftDevice, rightDevice, paramsDevice);
	checkCudaError(cudaDeviceSynchronize(), "Pixelwise device synchronize");
}

__global__ void PixelwiseCostCalculationKernel(costType *costDevice, cuda::PtrStepSz<unsigned __int8> leftMat, cuda::PtrStepSz<unsigned __int8> rightMat, algorithmParameters *paramsDevice)
{
	short int blockRow, blockCol;
	float sad = 0;

	int row = blockIdx.x,
		col = blockIdx.y,
		d = threadIdx.x;

	blockRow = -matchingCostBlockSize / 2;
#pragma unroll
	while (blockRow <= matchingCostBlockSize / 2)
	{
		blockCol = -matchingCostBlockSize / 2;
#pragma unroll
		while (blockCol <= matchingCostBlockSize / 2)
		{
			if (isPixelWithinBounds(row + blockRow, col + blockCol, paramsDevice) && isPixelWithinBounds(row + blockRow, col + blockCol - d, paramsDevice))
				sad += abs(leftMat(row + blockRow, col + blockCol) - rightMat(row + blockRow, col + blockCol - d));
			else if (isPixelWithinBounds(row + blockRow, col + blockCol, paramsDevice))
				sad += leftMat(row + blockRow, col + blockCol);
			++blockCol;
		}
		++blockRow;
	}

	//if (isPixelWithinBounds(row, col, paramsDevice) && isPixelWithinBounds(row, col - d, paramsDevice))
	//	sad += abs(leftMat(row, col) - rightMat(row, col - d));

	size_t elementIndex = (row * paramsDevice->nColsXDisparityRange);
	elementIndex += (col * paramsDevice->disparityRange);
	elementIndex += d;

	float scaledCost = (sad / 4590) * (1023 - -1024) + -1024;
	costDevice[elementIndex] = (costType) scaledCost;
	//costDevice[elementIndex] = ((sad - -1024) / (1023 - -1024)) * (32767 - -32768) + -32768;



	//// Variables for the equation
	//costType beforeRValue, rValue, afterRValue;
	//costType beforeLValue, lValue, afterLValue;
	//costType iRMinus, iRPlus, iRMax, iRMin;
	//costType iLMinus, iLPlus, iLMax, iLMin;

	//// Calculating the equation	
	//int rCol = col - d < 0 ? 0 : col - d;
	//
	//beforeRValue = rCol - 1 < 0 ? rightMat(row, rCol) : rightMat(row, rCol - 1);
	//rValue = rightMat(row, rCol);
	//afterRValue = rCol == paramsDevice->imageSectionWidth - 1 ? afterRValue = rightMat(row, rCol) : afterRValue = rightMat(row, rCol + 1);

	//iRMinus = (rValue + beforeRValue) / 2;
	//iRPlus = (rValue + afterRValue) / 2;
	//iRMax = MAX(MAX(rValue, iRMinus), iRPlus);
	//iRMin = MIN(MIN(rValue, iRMinus), iRPlus);

	//beforeLValue = (col == 0 ? leftMat(row, col) : leftMat(row, col - 1));
	//lValue = leftMat(row, col);
	//afterLValue = (col == paramsDevice->imageSectionWidth - 1 ? leftMat(row, col) : leftMat(row, col + 1));

	//iLMinus = (lValue + beforeLValue) / 2;
	//iLPlus = (lValue + afterLValue) / 2;
	//iLMax = MAX(MAX(lValue, iLMinus), iLPlus);
	//iLMin = MIN(MIN(lValue, iLMinus), iLPlus);

	//size_t elementIndex = (row * paramsDevice->nColsXDisparityRange);
	//elementIndex += (col * paramsDevice->disparityRange);
	//elementIndex += d;
	//costDevice[elementIndex] = MIN(MAX(MAX(0, lValue - iRMax), iRMin - lValue),
	//	MAX(MAX(0, rValue - iLMax), iLMin - rValue));
}

void SGM::AggregateCosts()
{
	// Below are various flags to make kernel launch easier and use just two kernels for all the paths

	int rowNotNeeded = -1,	// If rows are parallelized, then rows need not be sent to the kernel
		colNotNeeded = -1;	// Similarly, if columns are parallelized, then columns need not be sent to the kernel

	int	rowDifferenceNegative = -1,	// These specify the previous path
		rowDifferenceZero = 0,
		rowDifferencePositive = 1,

		colDifferenceNegative = -1,
		colDifferenceZero = 0,
		colDifferencePositive = 1;

	bool rowAndColToBeSubtracted = true;	// Required for the diagonal slice logic

	numberOfThreads.x = paramsHost->disparityRange;

	checkCudaError(cudaDeviceSynchronize(), "Aggregate Costs Device Synchronize");

	dim3 numberofBlocksForAllRows, numberofBlocksForAllColumns, numberofBlocksForAllSlices;

	// When the rows are parallelized
	numberofBlocksForAllRows.x = paramsHost->imageSectionHeight;
	numberofBlocksForAllRows.y = 1;
	numberofBlocksForAllRows.z = 1;

	// When the columns are parallelized
	numberofBlocksForAllColumns.x = 1;
	numberofBlocksForAllColumns.y = paramsHost->imageSectionWidth;
	numberofBlocksForAllColumns.z = 1;

	// When the diagonal slices are parallelized
	numberofBlocksForAllSlices.x = (paramsHost->imageSectionHeight < paramsHost->imageSectionWidth) ? paramsHost->imageSectionHeight : paramsHost->imageSectionWidth;
	numberofBlocksForAllSlices.y = 1;
	numberofBlocksForAllSlices.z = 1;

	dim3 allRowsColumns;
	allRowsColumns.x = paramsHost->imageSectionHeight;
	allRowsColumns.y = paramsHost->imageSectionWidth;

	/* All paths run sequentially */

	int row, col, slice, numberOfSlices = paramsHost->imageSectionWidth + paramsHost->imageSectionHeight - 1;

	AggregateCostsAddCostsKernel <<< allRowsColumns, numberOfThreads >>> (thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath.data()), paramsDevice);

	// Path 1: 0, -1
	for (col = 0; col < paramsHost->imageSectionWidth; ++col)
	{
		AggregateCostsNonDiagonalKernel <<< numberofBlocksForAllRows, numberOfThreads >>> (rowNotNeeded, col, rowDifferenceZero, colDifferenceNegative, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath.data()), paramsDevice, leftDevice);
		AggregateCostsSetPreviousMinimumsNonDiagonalKernel <<< numberofBlocksForAllRows, 1 >>> (rowNotNeeded, col, thrust::raw_pointer_cast(aggregatedCostsPath.data()), paramsDevice);
	}

	// Path 2: -1, -1
	for (slice = 0; slice < numberOfSlices; ++slice)
	{
		AggregateCostsDiagonalKernel <<< numberofBlocksForAllSlices, numberOfThreads >>>(slice, !rowAndColToBeSubtracted, rowDifferenceNegative, colDifferenceNegative, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath.data()), paramsDevice, leftDevice);
		AggregateCostsSetPreviousMinimumsDiagonalKernel <<< numberofBlocksForAllSlices, 1 >>> (slice, !rowAndColToBeSubtracted, thrust::raw_pointer_cast(aggregatedCostsPath.data()), paramsDevice);
	}

	// Path 3: -1, 0
	for (row = 0; row < paramsHost->imageSectionHeight; ++row)
	{
		AggregateCostsNonDiagonalKernel <<< numberofBlocksForAllColumns, numberOfThreads >>>(row, colNotNeeded, rowDifferenceNegative, colDifferenceZero, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath.data()), paramsDevice, leftDevice);
		AggregateCostsSetPreviousMinimumsNonDiagonalKernel <<< numberofBlocksForAllColumns, 1 >>>(row, colNotNeeded, thrust::raw_pointer_cast(aggregatedCostsPath.data()), paramsDevice);
	}

	// Path 4: -1, 1
	for (slice = paramsHost->imageSectionWidth - 1; slice > -paramsHost->imageSectionHeight; --slice)
	{
		AggregateCostsDiagonalKernel <<< numberofBlocksForAllSlices, numberOfThreads >>>(slice, rowAndColToBeSubtracted, rowDifferenceNegative, colDifferencePositive, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath.data()), paramsDevice, leftDevice);
		AggregateCostsSetPreviousMinimumsDiagonalKernel <<< numberofBlocksForAllSlices, 1 >>>(slice, rowAndColToBeSubtracted, thrust::raw_pointer_cast(aggregatedCostsPath.data()), paramsDevice);
	}

	// Path 5: 0, 1
	for (col = paramsHost->imageSectionWidth - 1; col >= 0; --col)
	{
		AggregateCostsNonDiagonalKernel <<< numberofBlocksForAllRows, numberOfThreads >>>(rowNotNeeded, col, rowDifferenceZero, colDifferencePositive, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath.data()), paramsDevice, leftDevice);
		AggregateCostsSetPreviousMinimumsNonDiagonalKernel <<< numberofBlocksForAllRows, 1 >>>(rowNotNeeded, col, thrust::raw_pointer_cast(aggregatedCostsPath.data()), paramsDevice);
	}

	// Path 6: 1, 1
	for (slice = numberOfSlices - 1; slice >= 0; --slice)
	{
		AggregateCostsDiagonalKernel <<< numberofBlocksForAllSlices, numberOfThreads >>>(slice, !rowAndColToBeSubtracted, rowDifferencePositive, colDifferencePositive, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath.data()), paramsDevice, leftDevice);
		AggregateCostsSetPreviousMinimumsDiagonalKernel <<< numberofBlocksForAllSlices, 1 >>>(slice, !rowAndColToBeSubtracted, thrust::raw_pointer_cast(aggregatedCostsPath.data()), paramsDevice);
	}

	// Path 7: 1, 0
	for (row = paramsHost->imageSectionHeight - 1; row >= 0; --row)
	{
		AggregateCostsNonDiagonalKernel <<< numberofBlocksForAllColumns, numberOfThreads >>>(row, colNotNeeded, rowDifferencePositive, colDifferenceZero, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath.data()), paramsDevice, leftDevice);
		AggregateCostsSetPreviousMinimumsNonDiagonalKernel <<< numberofBlocksForAllColumns, 1 >>>(row, colNotNeeded, thrust::raw_pointer_cast(aggregatedCostsPath.data()), paramsDevice);
	}

	// Path 8: 1, -1
	for (slice = -(paramsHost->imageSectionHeight - 1); slice < paramsHost->imageSectionWidth; ++slice)
	{
		AggregateCostsDiagonalKernel <<< numberofBlocksForAllSlices, numberOfThreads >>>(slice, rowAndColToBeSubtracted, rowDifferencePositive, colDifferencePositive, thrust::raw_pointer_cast(cost.data()), thrust::raw_pointer_cast(aggregatedCostsPath.data()), paramsDevice, leftDevice);
		AggregateCostsSetPreviousMinimumsDiagonalKernel <<< numberofBlocksForAllSlices, 1 >>>(slice, rowAndColToBeSubtracted, thrust::raw_pointer_cast(aggregatedCostsPath.data()), paramsDevice);
	}

	checkCudaError(cudaDeviceSynchronize(), "Aggregation All Paths Synchronization");
}

__global__ void AggregateCostsSetPreviousMinimumsNonDiagonalKernel(int row, int col, costType *aggregatedCostsDevice, algorithmParameters *paramsDevice)
{
	// If the row or column are supplied or not
	row = row == -1 ? blockIdx.x : row;
	col = col == -1 ? blockIdx.y : col;

	size_t pixelIndex = (row * paramsDevice->imageSectionWidth * (paramsDevice->disparityRange + 1)) + (col * (paramsDevice->disparityRange + 1));

	aggregatedCostsDevice[pixelIndex + paramsDevice->disparityRange] = INT16_MAX;

	//costType *result = thrust::min_element(thrust::device, aggregatedCostsDevice + pixelIndex, aggregatedCostsDevice + pixelIndex + paramsDevice->disparityRange);
	//aggregatedCostsDevice[pixelIndex + paramsDevice->disparityRange] = *result;

	for (int d = paramsDevice->disparityRange - 1; d >= 0; --d)
		if (aggregatedCostsDevice[pixelIndex + paramsDevice->disparityRange] > aggregatedCostsDevice[pixelIndex + d])
			aggregatedCostsDevice[pixelIndex + paramsDevice->disparityRange] = aggregatedCostsDevice[pixelIndex + d];

}

__global__ void AggregateCostsSetPreviousMinimumsDiagonalKernel(int slice, bool rowAndColToBeSubtracted, costType *aggregatedCostsDevice, algorithmParameters *paramsDevice)
{
	int sliceBlockSize = (paramsDevice->imageSectionHeight < paramsDevice->imageSectionWidth) ? paramsDevice->imageSectionHeight : paramsDevice->imageSectionWidth;

	int row = blockIdx.x, col = 0;

	while (row >= 0 && row < paramsDevice->imageSectionHeight)
	{
		if (rowAndColToBeSubtracted)
			col = row - slice;
		else if (!rowAndColToBeSubtracted)
			col = slice - row;

		if (col < 0 || col >= paramsDevice->imageSectionWidth)
			return;

		if (!isPixelWithinBounds(row, col, paramsDevice))
			return;

		if ((!rowAndColToBeSubtracted && (row + col == slice)) || (rowAndColToBeSubtracted && (row - col == slice)))
		{
			size_t pixelIndex = (row * paramsDevice->imageSectionWidth * (paramsDevice->disparityRange + 1)) + (col * (paramsDevice->disparityRange + 1));
			
			aggregatedCostsDevice[pixelIndex + paramsDevice->disparityRange] = INT16_MAX;
			
			//costType *result = thrust::min_element(thrust::device, aggregatedCostsDevice + pixelIndex, aggregatedCostsDevice + pixelIndex + paramsDevice->disparityRange);
			//aggregatedCostsDevice[pixelIndex + paramsDevice->disparityRange] = *result;
			
			for (int d = paramsDevice->disparityRange - 1; d >= 0; --d)
				if (aggregatedCostsDevice[pixelIndex + paramsDevice->disparityRange] > aggregatedCostsDevice[pixelIndex + d])
					aggregatedCostsDevice[pixelIndex + paramsDevice->disparityRange] = aggregatedCostsDevice[pixelIndex + d];
		}
		row += sliceBlockSize;
	}
}

__global__ void AggregateCostsAddCostsKernel(costType *costDevice, costType *aggregatedCostsDevice, algorithmParameters *paramsDevice)
{
	int row = blockIdx.x,
		col = blockIdx.y,
		d = threadIdx.x;

	size_t elementIndexS = (row * paramsDevice->imageSectionWidth * (paramsDevice->disparityRange + 1)) + (col * (paramsDevice->disparityRange + 1)) + d;
	size_t elementIndexC = (row * paramsDevice->imageSectionWidth * paramsDevice->disparityRange) + (col * paramsDevice->disparityRange) + d;

	aggregatedCostsDevice[elementIndexS] += costDevice[elementIndexC];
	aggregatedCostsDevice[(row * paramsDevice->imageSectionWidth * (paramsDevice->disparityRange + 1)) + (col * (paramsDevice->disparityRange + 1)) + paramsDevice->disparityRange] += INT16_MAX;
}

__global__ void AggregateCostsNonDiagonalKernel(int row, int col, int rowDifference, int colDifference, costType *costDevice, costType *aggregatedCostsDevice, algorithmParameters *paramsDevice, cv::cuda::PtrStepSz<unsigned __int8> leftMat)
{
	// If the row or column are supplied or not
	row = row == -1 ? blockIdx.x : row;
	col = col == -1 ? blockIdx.y : col;

	int d = threadIdx.x;
	size_t elementIndexS = (row * paramsDevice->imageSectionWidth * (paramsDevice->disparityRange + 1)) + (col * (paramsDevice->disparityRange + 1)) + d;
	size_t elementIndexC = (row * paramsDevice->imageSectionWidth * paramsDevice->disparityRange) + (col * paramsDevice->disparityRange) + d;

	//aggregatedCostsDevice[elementIndexS] += costDevice[elementIndexC];

	if (isPixelWithinBounds(row + rowDifference, col + colDifference, paramsDevice))
	{
		float gradient = abs(leftMat(row, col) - leftMat(row + rowDifference, col + colDifference));
		float penalty = paramsDevice->p2 / gradient > paramsDevice->p1 ? paramsDevice->p2 / gradient : paramsDevice->p1;
		aggregatedCostsDevice[elementIndexS] += AggregateCostsPreviousPathKernel(((row + rowDifference) * paramsDevice->imageSectionWidth * (paramsDevice->disparityRange + 1)) + ((col + colDifference) * (paramsDevice->disparityRange + 1)), d, aggregatedCostsDevice, paramsDevice, penalty);
	}
}

__global__ void AggregateCostsDiagonalKernel(int slice, bool rowAndColToBeSubtracted, int rowDifference, int colDifference, costType *costDevice, costType *aggregatedCostsDevice, algorithmParameters *paramsDevice, cv::cuda::PtrStepSz<unsigned __int8> leftMat)
{
	int sliceBlockSize = (paramsDevice->imageSectionHeight < paramsDevice->imageSectionWidth) ? paramsDevice->imageSectionHeight : paramsDevice->imageSectionWidth;

	int row = blockIdx.x, col = 0;

	while (row >= 0 && row < paramsDevice->imageSectionHeight)
	{
		if (rowAndColToBeSubtracted)
			col = row - slice;
		else if (!rowAndColToBeSubtracted)
			col = slice - row;

		if (col < 0 || col >= paramsDevice->imageSectionWidth)
			return;

		if (!isPixelWithinBounds(row, col, paramsDevice))
			return;

		if ((!rowAndColToBeSubtracted && (row + col == slice)) || (rowAndColToBeSubtracted && (row - col == slice)))
		{
			int d = threadIdx.x;
			size_t elementIndexS = (row * paramsDevice->imageSectionWidth * (paramsDevice->disparityRange + 1)) + (col * (paramsDevice->disparityRange + 1)) + d;
			size_t elementIndexC = (row * paramsDevice->nColsXDisparityRange) + (col * paramsDevice->disparityRange) + d;

			//aggregatedCostsDevice[elementIndexS] += costDevice[elementIndexC];

			if (isPixelWithinBounds(row + rowDifference, col + colDifference, paramsDevice))
			{
				float gradient = abs(leftMat(row, col) - leftMat(row + rowDifference, col + colDifference));
				float penalty = paramsDevice->p2 / gradient > paramsDevice->p1 ? paramsDevice->p2 / gradient : paramsDevice->p1;
				aggregatedCostsDevice[elementIndexS] += AggregateCostsPreviousPathKernel(((row + rowDifference) * paramsDevice->imageSectionWidth * (paramsDevice->disparityRange + 1)) + ((col + colDifference) * (paramsDevice->disparityRange + 1)), d, aggregatedCostsDevice, paramsDevice, penalty);
			}
		}

		row += sliceBlockSize;
	}
}

__device__ costType AggregateCostsPreviousPathKernel(size_t pixelIndex, int currentDisparity, costType *aggregatedCostsDevice, algorithmParameters *paramsDevice, float gradientPenalty)
{
	costType c1 = aggregatedCostsDevice[pixelIndex + currentDisparity],
		c2 = (currentDisparity - 1) >= 0 ? aggregatedCostsDevice[pixelIndex + currentDisparity - 1] + paramsDevice->p1 : INT16_MAX,
		c3 = (currentDisparity + 1) < paramsDevice->disparityRange - 1 ? aggregatedCostsDevice[pixelIndex + currentDisparity + 1] + paramsDevice->p1 : INT16_MAX,
		c4 = aggregatedCostsDevice[pixelIndex + paramsDevice->disparityRange];

	//c4 = aggregatedCostsDevice[pixelIndex];
	//for (int i = 1; i < paramsDevice->disparityRange; ++i)
	//	if (c4 > aggregatedCostsDevice[pixelIndex + i])
	//		c4 = aggregatedCostsDevice[pixelIndex + i];
	
	return MIN(
		MIN(c1, c2),
		MIN(c3, c4 + gradientPenalty)
	) - c4;
}

void SGM::DisparityCalculation()
{
	numberofBlocks.x = paramsHost->imageSectionHeight;
	numberofBlocks.y = paramsHost->imageSectionWidth;
	numberOfThreads.x = 1;

	checkCudaError(cudaDeviceSynchronize(), "Disparity Calculation Device Synchronize");

	DisparityCalculationKernel <<< numberofBlocks, numberOfThreads >>> (thrust::raw_pointer_cast(aggregatedCostsPath.data()), disparityDevice, paramsDevice);
	//SADDisparityCalculationKernel <<< numberofBlocks, numberOfThreads >>> (thrust::raw_pointer_cast(cost.data()), disparityDevice, paramsDevice);
	
	checkCudaError(cudaDeviceSynchronize(), "Disparity Calculation Complete");
}

__global__ void SADDisparityCalculationKernel(costType *costsDevice, cv::cuda::PtrStepSz<unsigned __int8> disparityMat, algorithmParameters *paramsDevice)
{
	int row = blockIdx.x,
		col = blockIdx.y;

	int pixelIndex = (row * paramsDevice->nColsXDisparityRange) + (col * (paramsDevice->disparityRange));

	float minD = 0;
	for (int d = 1; d < paramsDevice->disparityRange; ++d)
		if (costsDevice[pixelIndex + (int)minD] > costsDevice[pixelIndex + d])
			minD = d;
	minD = (minD / paramsDevice->disparityRange) * 255;
	disparityMat(row, col) = (unsigned __int8) minD;
}

__global__ void DisparityCalculationKernel(costType *aggregatedCostPathDevice, cv::cuda::PtrStepSz<unsigned __int8> disparityMat, algorithmParameters *paramsDevice)
{
	int row = blockIdx.x,
		col = blockIdx.y;

	int pixelIndex = (row * paramsDevice->imageSectionWidth * (paramsDevice->disparityRange + 1)) + (col * (paramsDevice->disparityRange + 1));

	float minD = 0;
	for (int d = 1; d < paramsDevice->disparityRange; ++d)
		if (aggregatedCostPathDevice[pixelIndex + (int)minD] > aggregatedCostPathDevice[pixelIndex + d])
			minD = d;
	minD = (minD / (paramsDevice->disparityRange - 1)) * 255;
	disparityMat(row, col) = (unsigned __int8) minD;
}

void SGM::StitchImageSections()
{
	checkCudaError(cudaDeviceSynchronize(), "Generate Map Device Synchronize");
	disparityDevice.download(disparitySectionHost);
	disparitySectionHost.copyTo(disparityHost(Rect(paramsHost->imageSectionStartX, paramsHost->imageSectionStartY, paramsHost->imageSectionWidth, paramsHost->imageSectionHeight)));
}

void SGM::PostProcessAndGenerateMap()
{
	// Median Filter
	medianBlur(disparityHost, disparityHost, 7);

	int specklePaintoverValue = 0,
		maxSpecklesize = 150,
		maxNeighborDifference = 16;
	//filterSpeckles(disparityHost, specklePaintoverValue, maxSpecklesize, maxNeighborDifference);

	dilate(disparityHost, disparityHost, Mat::ones(3, 3, CV_8U), Point(-1, -1), 1);

	// Normalizing to greyscale
	//normalize(disparityHost, disparityHost, 1, 255, NORM_MINMAX, CV_8UC1);

	// Generate Map
	imwrite(disparityFilePath, disparityHost);
}












/*********************************************************
The images are processed in multiple passes, each pass
processing a section of the image
**********************************************************/
void SGM::SplitImagesIntoSections()
{
	paramsHost->imageSectionStartX = 0;
	paramsHost->imageSectionStartY = paramsHost->imageSectionBeingProcessed * paramsHost->imageSectionHeight;

	Rect roi = Rect(paramsHost->imageSectionStartX, paramsHost->imageSectionStartY, paramsHost->imageSectionWidth, paramsHost->imageSectionHeight);
	leftSectionHost = leftHost(roi);
	rightSectionHost = rightHost(roi);
}

/*********************************************************
Filters applied:
- (Uncomment to apply) Gaussian filter
- Sobel Filter
**********************************************************/
void SGM::PreProcessAndUploadImagesToDevice()
{
	// Applying the Gaussian Blur
	//GaussianBlur(leftBlockHost, leftBlockHost, Size(3, 3), 0, 0);
	//GaussianBlur(rightBlockHost, leftBlockHost, Size(3, 3), 0, 0);

	//Sobel Operator
	/// Generate grad_x and grad_y
	int ddepth = CV_8UC1;
	int scale = 1;
	int delta = 0;
	Mat grad_xLeft, grad_yLeft;
	Mat grad_xRight, grad_yRight;

	Mat abs_grad_xLeft, abs_grad_yLeft;
	Mat abs_grad_xRight, abs_grad_yRight;

	/// Gradient X
	Sobel(leftSectionHost, grad_xLeft, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT);
	convertScaleAbs(grad_xLeft, abs_grad_xLeft);

	Sobel(rightSectionHost, grad_xRight, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT);
	convertScaleAbs(grad_xRight, abs_grad_xRight);

	/// Gradient Y
	Sobel(leftSectionHost, grad_yLeft, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT);
	convertScaleAbs(grad_yLeft, abs_grad_yLeft);

	Sobel(rightSectionHost, grad_yRight, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT);
	convertScaleAbs(grad_yRight, abs_grad_yRight);

	/// Total Gradient (approximate)
	addWeighted(abs_grad_xLeft, 0.5, abs_grad_yLeft, 0.5, 0, leftSectionHost);
	addWeighted(abs_grad_xRight, 0.5, abs_grad_yRight, 0.5, 0, rightSectionHost);

	leftDevice.upload(leftSectionHost);
	rightDevice.upload(rightSectionHost);
}

void SGM::ResetArrays()
{
	thrust::fill(cost.begin(), cost.end(), 0);
	thrust::fill(aggregatedCostsPath.begin(), aggregatedCostsPath.end(), 0);

	leftDevice.release();
	rightDevice.release();
}

void SGM::checkCudaError(cudaError_t cudaStatus, string message)
{
	if (cudaStatus != cudaSuccess)
	{
		cout << "Cuda Error in: " << message << " with error code: " << cudaGetErrorString(cudaStatus) << endl;
		exit(EXIT_FAILURE);
	}
}

SGM::~SGM()
{
	checkCudaError(cudaDeviceSynchronize(), "Deallocation");

	leftDevice.release();
	rightDevice.release();
	disparityDevice.release();

	checkCudaError(cudaFree(paramsDevice), "Freeing parameter struct");

	cost.clear(); cost.shrink_to_fit();
	aggregatedCostsPath.clear(); aggregatedCostsPath.shrink_to_fit();
}

